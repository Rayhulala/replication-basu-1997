	(1)	(2)	(3)	(4)
	est1	est2	est3	est4
VARIABLES	x_delta	x_delta	x_delta	x_delta
				
d_ret_fy		-0.087***		
		(0.016)		
x_delta_1	-0.094***	-0.097***	-0.088	-0.097***
	(0.031)	(0.026)	(0.067)	(0.026)
p_xd1_r		0.009		
		(0.072)		
Constant	0.089***	0.120***	0.033**	0.120***
	(0.006)	(0.004)	(0.015)	(0.004)
				
Observations	51,587	51,587	18,590	32,997
R-squared	0.023	0.025	0.010	0.078
Robust standard errors in parentheses				
*** p<0.01, ** p<0.05, * p<0.1				
