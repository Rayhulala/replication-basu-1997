	(1)	(2)	(3)	(4)
	est1	est2	est3	est4
VARIABLES	cfo	cfo	cfo	cfo
				
d_ret_itan		-0.025***		
		(0.003)		
ret_itan	0.048***	-0.007	0.095***	-0.007
	(0.003)	(0.005)	(0.008)	(0.005)
p_ret_itan		0.102***		
		(0.009)		
Constant	0.126***	0.153***	0.129***	0.153***
	(0.001)	(0.002)	(0.002)	(0.002)
				
Observations	57,563	57,563	20,377	37,186
R-squared	0.008	0.014	0.013	0.000
Robust standard errors in parentheses				
*** p<0.01, ** p<0.05, * p<0.1				
