	(1)	(2)	(3)	(4)
	est1	est2	est3	est4
VARIABLES	cfo	cfo	cfo	cfo
				
d_ret_itan		-0.025***		
		(0.003)		
ret_itan	0.049***	-0.010**	0.101***	-0.010**
	(0.003)	(0.005)	(0.007)	(0.005)
p_ret_itan		0.110***		
		(0.008)		
Constant	0.124***	0.153***	0.128***	0.153***
	(0.001)	(0.002)	(0.002)	(0.002)
				
Observations	54,739	54,739	19,656	35,083
R-squared	0.009	0.015	0.015	0.000
Robust standard errors in parentheses				
*** p<0.01, ** p<0.05, * p<0.1				
