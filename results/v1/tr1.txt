	(1)	(2)	(3)	(4)
	est1	est2	est3	est4
VARIABLES	x	x	x	x
				
d_ret_itan		-0.017***		
		(0.003)		
ret_itan	0.118***	0.035***	0.228***	0.035***
	(0.003)	(0.004)	(0.008)	(0.004)
p_ret_itan		0.193***		
		(0.009)		
Constant	0.036***	0.075***	0.058***	0.075***
	(0.001)	(0.002)	(0.002)	(0.002)
				
Observations	60,595	60,595	21,734	38,861
R-squared	0.055	0.072	0.062	0.003
Robust standard errors in parentheses				
*** p<0.01, ** p<0.05, * p<0.1				
