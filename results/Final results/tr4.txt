	(1)	(2)	(3)	(4)
	est1	est2	est3	est4
VARIABLES	cfoi	cfoi	cfoi	cfoi
				
d_ret_itan		-0.019***		
		(0.004)		
ret_itan	0.023***	0.002	0.022**	0.002
	(0.003)	(0.006)	(0.009)	(0.006)
p_ret_itan		0.020*		
		(0.011)		
Constant	0.069***	0.081***	0.062***	0.081***
	(0.001)	(0.002)	(0.003)	(0.002)
				
Observations	57,563	57,563	20,377	37,186
R-squared	0.001	0.002	0.000	0.000
Robust standard errors in parentheses				
*** p<0.01, ** p<0.05, * p<0.1				
