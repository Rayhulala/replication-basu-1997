***Replication of Basu 1997***
***Ruishen Zhang***

* 1. Preliminary
use "F:\hulala coding\replication-basu-1997\Data\df.dta", clear

//drop variable not used in analysis
 
drop x_sum_4 ret_3m car x_2 d_x_2

gen car12 = car_1 + car_2

//label control variables
label variable index "Raw dataset row index"
label variable fyear "fiscal year (categorical)"
label variable cfo "cash flow form operation"
label variable cfoi "cash flow form operation and investment"
label variable x "earnings per share scaled by year opening price"
label variable xe "earnings per share before extraordinary items scaled by year opening price"
label variable x_1 "lagged x (earnings)"
label variable x_delta "change of earnnigs (per share and scaled by opening price)"
label variable x_delta_1 "last year earnings  chage (lagged x_delta)"
label variable xa "earnings pershare scaled be net assets"
label variable x_year_adj "cumulative earnings from last foyr years adjusted by fiscal year mean"
label variable x_adj "earnings from adjusted by fiscal year mean"
label variable prc "price"
label variable ret "holding period return"
label variable ewretd "Equal-Weighted Return (includes distributions)"
label variable d_12month "check 12 month consecutive data"
label variable d_9month "check 9 month consecutive data"
label variable d_45month "check 45 month consecutive data"
label variable ret_fy "fiscal year return"
label variable ret_itan "inter-announcement period return"
label variable ret_fy_mktadj "market adjusted fiscal year return"
label variable ret_itan_mktadj "market adjusted inter-announcement period return"
label variable bhret_4y "4 year buy and hold return"
label variable bhret_itan "inter-announcement period buy and hold return"
label variable car_1 "abormal return for first month after fical year end"
label variable car_2 "abormal return for second month after fical year end"
label variable ret_ptile "retrun percentile"
label variable xe_ptile "percentile for xe"
label variable xa_ptile "percentile for xa"
label variable cfo_ptile "Cash flow from operation percentile"
label variable cfoi_ptile "Cash flow from operation and investment percentile"
label variable year "fiscal year (float) for yaer-wise regression"
label variable car12 "abnormal return for first two months after fiscal year end"
label variable d_ret_fy "fiscal year return patrition"
label variable d_ret_itan "inter-anouncement period return partition"
label variable d_ret_fy_mktadj "market adjusted fiscal year return partition"
label variable d_ret_itan_mktadj "market adjusted inter-anouncement period return partition"
label variable d_bhret_4y "4 year buy and hold return partition"
label variable d_bhret_itan "market adjusted inter-anouncement period buy and hold return partition"
label variable d_bhret_4y "4 year adjusted buy and hold return partition"
label variable d_x_1 "last year earning level partition"
label variable d_x_delta_1 "last year earning change partition"
label variable d_x "current year earning partition"


// keep observations have 9 consective month return data at fiscal year end
keep if d_9month == 1

** Return as good/bad news proxy				
global ret_fy " d_ret_fy ret_fy p_ret_fy "
global ret_itan " d_ret_itan ret_itan p_ret_itan "
global ret_fy_mktadj " d_ret_fy_mktadj ret_fy_mktadj p_ret_fy_mktadj "
global ret_itan_mktadj " d_ret_itan_mktadj ret_itan_mktadj p_ret_itan_mktadj "
global bhret_4y " d_bhret_4y bhret_4y p_bhret_4y "
global bhret_itan " d_bhret_itan bhret_itan p_bhret_itan "

** Previous earning/ change in earning as news type proxy

gen p_xd1_x1d = d_x_delta_1 * x_delta_1
gen p_xd1_xd = d_x_1 * x_delta_1
gen p_xd1_r = d_ret_itan * x_delta_1
gen p_xd_xd = d_x_delta * x_delta


global change_p "d_x_delta_1 x_delta_1 p_xd1_x1d"
global level_p "d_x_1 x_delta_1 p_xd1_xd"
global return_p "d_ret_itan x_delta_1 p_xd1_r"

** Abnoraml Return on Earning Change
global car "d_x_delta x_delta p_xd_xd"

// Basu 1997 drops observations that have return, eps before extraordinary, ///
// and eps scaled by total assets in top and bottom 1%
global del "ret_ptile xe_ptile xa_ptile"

// For cash flow items, Basu 1997 drops top and bottom 1% observations
global del_cf "cfo_ptile cfoi_ptile"

// For abnormal return items, Basu 1997 drops top and bottom 1% observations

global del_ar "car_1 car_2 x_1_delta"
global del_radj "ret_adj_1 ret_adj_2 x_1_delta"
// Mark the order of table outputs


* 2. Regression programs


// Regression for earnings
***Sample selection /Observation drop varies accross regression***

capture program drop test_x
	program define test_x
	args dv iv1 iv2 iv3
	
		eststo clear
		local counter=0
		
		preserve
			foreach var in `dv' `iv2'{
				drop if missing(`var')
				}
			
			foreach var in $del{
				drop if `var' > 0.99 
				drop if `var' < 0.01
				}
				
			sum `dv' `iv1' `iv2' `iv3'

			local counter = `counter'+1 		
				quietly eststo est`counter': reg `dv' `iv2', vce(robust)
				quietly estadd local dv "`dv'", replace
		
			local counter = `counter'+1 		
				quietly eststo est`counter': reg `dv' `iv1' `iv2' `iv3', vce(robust)
				quietly estadd local dv "`dv'", replace
			
			local counter = `counter'+1 		
				quietly eststo est`counter': reg `dv' `iv2' if `iv1' == 1, vce(robust)
				quietly estadd local dv "`dv'", replace
				
			local counter = `counter'+1 		
				quietly eststo est`counter': reg `dv' `iv2' if `iv1' == 0, vce(robust)
				quietly estadd local dv "`dv'", replace
			
			global t_counter = $t_counter +1
			
			esttab est1 est2 est3 est4, order(*_cons* *d_* ** *p_*) s(N r2_a) star(* 0.10 ** 0.05 *** 0.01)	
			outreg2 [est1 est2 est3 est4] using tr$t_counter , replace excel  bdec(3)  sdec(3)
			outreg2 [est1 est2 est3 est4] using tr$t_counter , replace tex(frag)  bdec(3)  sdec(3)
			restore

	end

	
// Regression for  abnormal return
capture program drop test_ar
	program define test_ar
	args dv1 dv2 dv3 iv1 iv2 iv3
	
		eststo clear
		local counter = 0
		
		preserve

			foreach var in `dv' `iv2'{
				drop if missing(`var')
				}
			foreach var in $del{
				drop if `var' > 0.99 
				drop if `var' < 0.01
				}
				
				
			local counter = `counter'+1 		
				quietly eststo est`counter': reg `dv1' `iv2', vce(robust)
				quietly estadd local dv "`dv'", replace

			local counter = `counter'+1 		
				quietly eststo est`counter': reg `dv2' `iv2', vce(robust)
				quietly estadd local dv "`dv'", replace
			
			local counter = `counter'+1 		
				quietly eststo est`counter': reg `dv3' `iv2', vce(robust)
				quietly estadd local dv "`dv'", replace
			
			local counter = `counter'+1 		
				quietly eststo est`counter': reg `dv1' `iv1' `iv2' `iv3', vce(robust)
				quietly estadd local dv "`dv'", replace
			
			local counter = `counter'+1 		
				quietly eststo est`counter': reg `dv2' `iv1' `iv2' `iv3', vce(robust)
				quietly estadd local dv "`dv'", replace
		
			local counter = `counter'+1 		
				quietly eststo est`counter': reg `dv3' `iv1' `iv2' `iv3', vce(robust)
				quietly estadd local dv "`dv'", replace
			
			global t_counter = $t_counter +1
			
			esttab est1 est2 est3 est4 est5 est6,order(*_cons* *d_* ** *p_*) s(N r2_a) star(* 0.10 ** 0.05 *** 0.01)	
			outreg2 [est1 est2 est3 est4 est5 est6] using tr$t_counter , replace excel bdec(3)  sdec(3)
			outreg2 [est1 est2 est3 est4 est5 est6] using tr$t_counter , replace tex(frag) bdec(3)  sdec(3)
			restore

	end

	
// Regression for cash flow
capture program drop test_cf
	program define test_cf
	args dv iv1 iv2 iv3
	
		eststo clear
		local counter = 0
		
		preserve
			foreach var in `dv' `iv2'{
				drop if missing(`var')
				}
				
			foreach var in $del $del_cf{
				drop if `var' > 0.99 
				drop if `var' < 0.01
				}
				
			sum `dv' `iv1' `iv2' `iv3'

			local counter = `counter'+1 		
				quietly eststo est`counter': reg `dv' `iv2', vce(robust)
				quietly estadd local dv "`dv'", replace
		
			local counter = `counter'+1 		
				quietly eststo est`counter': reg `dv' `iv1' `iv2' `iv3', vce(robust)
				quietly estadd local dv "`dv'", replace
			
			local counter = `counter'+1 		
				quietly eststo est`counter': reg `dv' `iv2' if `iv1' == 1, vce(robust)
				quietly estadd local dv "`dv'", replace
				
			local counter = `counter'+1 		
				quietly eststo est`counter': reg `dv' `iv2' if `iv1' == 0, vce(robust)
				quietly estadd local dv "`dv'", replace
			
			global t_counter = $t_counter +1
			
			esttab est1 est2 est3 est4, order(*_cons* *d_* ** *p_*) s(N r2_a) star(* 0.10 ** 0.05 *** 0.01)	
			outreg2 [est1 est2 est3 est4] using tr$t_counter , replace excel bdec(3)  sdec(3)
			outreg2 [est1 est2 est3 est4] using tr$t_counter , replace tex(frag) bdec(3)  sdec(3)
			restore

	end
	
**************************Analysis********************************	
cd "F:\hulala coding\replication-basu-1997\results\v5"

log using "Result.smcl", replace
global t_counter = 0

eststo clear

keep if year > 1962

*3. Table 1	
	test_x x $ret_itan
	test_x x_adj $ret_itan_mktadj
	test_x x $ret_fy

*4 Table 2 Panel A & B
	test_cf cfoi $ret_itan
	test_cf cfo $ret_itan
	test_x xe $ret_itan
	
*5 Table 3
	test_x x_delta $change_p
	test_x x_delta $level_p
	test_x x_delta $return_p
	
*6 Table 4
	preserve
		//keep if d_45month == 1

		test_ar car12 car_1 car_2 $car
		
	restore
*7 Table 5
	test_x x_year_adj $bhret_4y
*8 Table 6
	gen d_67_75 = (year < 1976 & year > 1966) if !missing(year)
	gen d_76_82 = (year < 1983 & year > 1975) if !missing(year)
	gen d_83_90 = (year < 1991 & year > 1982) if !missing(year)
	
	global d_year "d_67_75 d_76_82 d_83_90"
	
	quietly eststo est1: reg x_adj $bhret_itan d_67_75##c.bhret_itan d_76_82##c.bhret_itan d_83_90##c.bhret_itan /// 
		d_67_75##d_bhret_itan d_76_82##d_bhret_itan d_83_90##d_bhret_itan ///
		d_67_75##c.p_bhret_itan d_76_82##c.p_bhret_itan d_83_90##c.p_bhret_itan, vce(robust)
	
	global t_counter = $t_counter +1
	esttab est1,s(N r2_a fixed dv) star(* 0.10 ** 0.05 *** 0.01)
	outreg2 [est1] using tr$t_counter , replace excel bdec(3)  sdec(3)
	outreg2 [est1] using tr$t_counter , replace tex(frag) bdec(3)  sdec(3)
	
*9 Table 2 Panel C

//Restricted
	eststo clear
	
	constraint 1 d_ret_itan = 0 
	constraint 2 p_ret_itan = 0
	preserve
			foreach var in cfoi cfo xe $ret_itan{
				drop if missing(`var')
				}
			
			foreach var in $del $del_cf{
				drop if `var' > 0.99 
				drop if `var' < 0.01
				}
				
	quietly eststo est11: cnsreg cfoi $ret_itan, constraints(1,2) 
	quietly eststo est12: cnsreg cfo $ret_itan, constraints(1,2)		
	quietly eststo est13: cnsreg xe $ret_itan, constraints(1,2)
	
	suest est11 est12 est13
	test [est11 = est12 = est13]: ret_itan
	estadd scalar chi_01 = r(chi2)
	estadd scalar p_01 = r(p)	
	
	test [est11 = est12]: ret_itan
	estadd scalar chi_02 = r(chi2)
	estadd scalar p_02 = r(p)
	
	test [est11 = est13]: ret_itan
	estadd scalar chi_03 = r(chi2)
	estadd scalar p_03 = r(p)
	
	test [est12 = est13]: ret_itan
	estadd scalar chi_04 = r(chi2)
	estadd scalar p_04 = r(p)
	
	estout using test1.txt, style(tex) stats(chi_01 p_01 chi_02 p_02 chi_03 p_03 chi_04 p_04)
	
	restore
	
	eststo clear
	
//Unrestricted
	
	preserve
			foreach var in cfoi cfo xe $ret_itan{
				drop if missing(`var')
				}
			
			foreach var in $del $del_cf{
				drop if `var' > 0.99 
				drop if `var' < 0.01
				}
				
	sureg (cfoi $ret_itan) (cfo $ret_itan) (xe $ret_itan)
	test [cfoi = cfo = xe]: d_ret_itan
	estadd scalar chi_11 = r(chi2)
	estadd scalar p_11 = r(p)
	
	test [cfoi = cfo = xe]: p_ret_itan
	estadd scalar chi_12 = r(chi2)
	estadd scalar p_12 = r(p)
	
	test [cfoi = cfo]: ret_itan
	estadd scalar chi_13 = r(chi2)
	estadd scalar p_13 = r(p)
	
	test [cfoi = cfo]: p_ret_itan
	estadd scalar chi_14 = r(chi2)
	estadd scalar p_14 = r(p)
	
	test [cfoi = xe]: ret_itan
	estadd scalar chi_15 = r(chi2)
	estadd scalar p_15 = r(p)
	
	test [cfoi = xe]: p_ret_itan
	estadd scalar chi_16 = r(chi2)
	estadd scalar p_16 = r(p)
	
	test [cfo = xe]: ret_itan
	estadd scalar chi_17 = r(chi2)
	estadd scalar p_17 = r(p)
	
	test [cfo = xe]: p_ret_itan
	estadd scalar chi_18 = r(chi2)
	estadd scalar p_18 = r(p)
	
	estout using test2.txt, style(tex) stats(chi_11 p_11 chi_12 p_12 chi_13 p_13 chi_14 p_14 ///
		chi_15 p_15 chi_16 p_16 chi_17 p_17 chi_18)
	
	restore

	
//	The figure
	
	local year_c = 1964
	
		while `year_c' < 1991{
			quietly eststo est`year_c': reg x_adj $bhret_itan if year == `year_c'
			
			outreg2 [est`year_c'] using fig , excel noaster bdec(3) sdec(3)
			local year_c = `year_c'+1
			}
			
	log close
	
	
	
