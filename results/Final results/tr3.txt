	(1)	(2)	(3)	(4)
	est1	est2	est3	est4
VARIABLES	x	x	x	x
				
d_ret_fy		-0.017***		
		(0.003)		
ret_fy	0.129***	0.063***	0.204***	0.063***
	(0.003)	(0.004)	(0.008)	(0.004)
p_ret_fy		0.140***		
		(0.009)		
Constant	0.035***	0.066***	0.049***	0.066***
	(0.001)	(0.002)	(0.002)	(0.002)
				
Observations	64,387	64,387	22,743	41,644
R-squared	0.063	0.072	0.045	0.009
Robust standard errors in parentheses				
*** p<0.01, ** p<0.05, * p<0.1				
