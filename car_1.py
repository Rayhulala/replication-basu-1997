
# coding: utf-8

# In[1]:

import numpy as np
import pandas as pd
import os
import deepdish as dd
import scipy
import statsmodels.api as sm


# In[2]:

data_dir = r'F:\hulala coding\Data\Repplication_Basu_1997'
os.chdir(data_dir)
stock_car = dd.io.load('stock_car.h5')


# In[3]:


# In[16]:
error = []
def car(i):
    try:
        X = sm.add_constant(stock_car.loc[i-59:i-1,'ewretd'])
        y = stock_car.loc[i-59:i-1,'ret']
        est = sm.OLS(y, X).fit()
        X_prime = sm.add_constant(stock_car.loc[i:i,'ewretd'])
        y_hat = est.predict(X_prime)[0]
        stock_car.loc[i, 'car'] = y_hat - stock_car.loc[i,'ret']
    except:
        print(i)
        error.append(i)

# In[22]:

df = stock_car[stock_car['d_60month'] == 60]


# In[24]:

lp = df['index']


# In[20]:

for i in lp:
    car(i)

# In[ ]:

dd.io.save('stock_car.h5', stock_car, compression = 'blosc')
stock_car.to_csv('finish.csv', index = False)

# In[ ]:




# In[ ]:




# In[ ]:



