	(1)	(2)	(3)	(4)
	est1	est2	est3	est4
VARIABLES	cfoi	cfoi	cfoi	cfoi
				
d_ret_itan		-0.011***		
		(0.004)		
ret_itan	-0.004	-0.022***	0.004	-0.022***
	(0.003)	(0.005)	(0.007)	(0.005)
p_ret_itan		0.026***		
		(0.009)		
Constant	-0.027***	-0.017***	-0.029***	-0.017***
	(0.001)	(0.002)	(0.003)	(0.002)
				
Observations	54,739	54,739	19,656	35,083
R-squared	0.000	0.001	0.000	0.001
Robust standard errors in parentheses				
*** p<0.01, ** p<0.05, * p<0.1				
